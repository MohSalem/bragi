//
//  Deserializable.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Deserializable Protocol provides interface for object model

protocol Deserializable {
    
    /// Initialize with an object model with deserialized data
    ///
    /// - parameter data: A json data to be serialized
    /// - throws: DeserializationError
    init(data: Data) throws
}
