//
//  Checklist.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Checklist object model represent a checklist with array of numbers retrieved from backend

class Checklist: Deserializable {
    
    // MARK: Variable properties
    
    var numbers: [Int]?         /// An array of Int property represents holds checklist coded binary numbers
    
    // MARK: Class initializers
    
    /// Initialize checklist object model with deserialized data
    ///
    /// - parameter data: A json data to be serialized
    /// - throws: DeserializationError
    required init(data: Data) throws {
        numbers = data["numbers"] as? [Int]
    }
}
