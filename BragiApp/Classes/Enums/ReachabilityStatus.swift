//
//  ReachabilityStatus.swift
//  BragiApp
//
//  Created by Mohamed Salem on 12.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Reachability status string Enum implements custom string convertible protocol

public enum ReachabilityStatus: String, CustomStringConvertible {
    
    case unreachable        /// Reachability status is unreachable
    case wifi               /// Reachability status is reachable via WiFi only
    case wwan               /// Reachability status is 3G or 4G data only
    
    /// Implementation for custom string convertible protocol to provide string description of reachability status
    public var description: String { return rawValue }
}
