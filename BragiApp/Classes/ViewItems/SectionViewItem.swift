//
//  SectionViewItem.swift
//  BragiApp
//
//  Created by Mohamed Salem on 10.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Section View Item represents table section for checklist view item

class SectionViewItem {
    
    // MARK: Variable properties
    
    var index: Int                      /// A Int property represents checklist section index
    var items: [RowViewItem] = []       /// An array of Row view items represention checklist row items
    
    /// A computed string property represents section title
    var title: String {
        return String.localize(key: "SectionTitle", index + 1)
    }
    
    // MARK:- Class initializers
    
    /// Initialize section view item with an index and array of row view items
    ///
    /// - parameter index: An index int represents section index
    /// - parameter [RowViewItem]: An array of Row view items represention checklist row items
    init(index: Int, items: [RowViewItem]) {
        
        self.index = index
        self.items = items
    }
    
    /// Initialize section view item with an index and array of checklist view items
    ///
    /// - parameter index: An index int represents section index
    /// - parameter [ChecklistViewItem]: An array of checklist view items to be filtered with section index
    init(index: Int, items: [ChecklistViewItem]) {
        
        self.index = index
        self.items = rowViewItems(for: items)
    }
    
    // MARK: Private functions
    
    private func rowViewItems(for checklistViewItems: [ChecklistViewItem]) -> [RowViewItem] {
        
        let rows = checklistViewItems.filter { $0.section == index }
        
        var rowViewItems: [RowViewItem] = []
        
        for row in rows {
            
            let rowViewItem = RowViewItem(checklistViewItem: row)
            
            rowViewItems.append(rowViewItem)
        }
        
        return rowViewItems
    }
}
