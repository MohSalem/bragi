//
//  RowViewItem.swift
//  BragiApp
//
//  Created by Mohamed Salem on 10.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Row View Item represents table row for checklist view item

class RowViewItem {
    
    // MARK: Variable properties
    
    var index: Int              /// A Int property represents checklist row index
    var isChecked: Bool         /// A Int property determines checklist checkmark status
    
    /// A computed string property represents row title
    var title: String {
        return String.localize(key: "RowTitle", index + 1)
    }
    
    // MARK:- Class initializers
    
    /// Initialize row view item with an index and checkmark status
    ///
    /// - parameter index: An index int represents row index
    /// - parameter isChecked: An bool determines if checkmark is checked
    init(index: Int, isChecked: Bool) {
        
        self.index = index
        self.isChecked = isChecked
    }
    
    /// Initialize row view item with a checklist view item
    ///
    /// - parameter checklistViewItem: An checklist view item to be processed to a row view item
    convenience init(checklistViewItem: ChecklistViewItem) {
        
        let index = checklistViewItem.item
        let isChecked = checklistViewItem.isChecked
        
        self.init(index: index, isChecked: isChecked)
    }
}
