//
//  ChecklistViewItem.swift
//  BragiApp
//
//  Created by Mohamed Salem on 10.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Checklist View Item encapsulates a deserialized number object model

class ChecklistViewItem {
    
    private let numberService: NumberProtocol = NumberService()
    
    // MARK: Variable properties
    
    var section: Int        /// A Int property represents checklist section index
    var item: Int           /// A Int property represents checklist row index
    var isChecked: Bool     /// A Int property determines checklist checkmark status
    
    // MARK:- Class initializers
    
    /// Initialize checklist view item with a decoded number
    ///
    /// - parameter number: A decoded int number encapsulating information for section and rows
    init(number: Int) {
        section = numberService.section(for: number)
        item = numberService.item(for: number)
        isChecked = numberService.checkmark(for: number)
    }
}
