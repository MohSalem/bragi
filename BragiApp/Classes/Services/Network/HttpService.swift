//
//  HttpService.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Http Service provides implementation for http url session protocol

class HttpService: HttpProtocol {
    
    let reachabilityService: ReachabilityProtocol? = ReachabilityService()
    let errorService: ErrorProtocol? = ErrorService()
    
    /// Http request url get handler.
    ///
    /// - parameter URL: The request’s url to get data from backend.
    /// - parameter completionHandler: The response completion block handler to handle response and error.
    func get(url: URL?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        guard let isConnectedToNetwork = reachabilityService?.isConnectedToNetwork,
                    isConnectedToNetwork == true
            else {
                errorService?.show(error: ReachabilityError.unreachable)
                return
        }
        
        if let url = url {
            let task = URLSession.shared.dataTask(with: url, completionHandler: completionHandler)
            task.resume()
        }
    }
}
