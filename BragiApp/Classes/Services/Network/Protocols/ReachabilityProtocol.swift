//
//  ReachabilityProtocol.swift
//  BragiApp
//
//  Created by Mohamed Salem on 12.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation
import SystemConfiguration

/// Reachability Protocol provides interface for reachability services

protocol ReachabilityProtocol {
    
    // MARK: Variable properties
    
    /// A Bool determines if there's a reachable internet connection available via WiFi or 4G
    var isConnectedToNetwork: Bool { get }
    
    /// A Bool determines if there's a reachable internet connection available via WiFi only
    var isReachableViaWiFi: Bool { get }
    
    /// Flags that indicate the reachability of a network node name or address, including whether a connection is required, and whether some user intervention might be required when establishing a connection.
    var flags: SCNetworkReachabilityFlags? { get }
    
    /// The specified node name or address can be reached via a transient connection, such as PPP.
    var transientConnection: Bool { get }
    
    /// The specified node name or address can be reached using the current network configuration.
    var isReachable: Bool { get }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established. If this flag is set, the kSCNetworkReachabilityFlagsConnectionOnTraffic flag, kSCNetworkReachabilityFlagsConnectionOnDemand flag, or kSCNetworkReachabilityFlagsIsWWAN flag is also typically set to indicate the type of connection required. If the user must manually make the connection, the kSCNetworkReachabilityFlagsInterventionRequired flag is also set.
    var connectionRequired: Bool { get }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established. Any traffic directed to the specified name or address will initiate the connection.
    var connectionOnTraffic: Bool { get }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established.
    var interventionRequired: Bool { get }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established. The connection will be established "On Demand" by the CFSocketStream programming interface (see CFStream Socket Additions for information on this). Other functions will not establish the connection.
    var connectionOnDemand: Bool { get }
    
    /// The specified node name or address is one that is associated with a network interface on the current system.
    var isLocalAddress: Bool { get }
    
    /// Network traffic to the specified node name or address will not go through a gateway, but is routed directly to one of the interfaces in the system.
    var isDirect: Bool { get }
    
    /// The specified node name or address can be reached via a cellular connection, such as EDGE or GPRS.
    var isWWAN: Bool { get }
    
    /// The specified node name or address can be reached using the current network configuration, but a connection must first be established. If this flag is set
    /// The specified node name or address can be reached via a transient connection, such as PPP.
    var isConnectionRequiredAndTransientConnection: Bool { get }
    
    // MARK:- Public functions
    
    /// Starts reachability service listening to reachability status changes and post notification observer
    /// - throws: ReachabilityError
    func start() throws
    
    /// Stop reachability service listening to reachability status changes and remove notification observer
    func stop()

    /// compares the current flags with the previous flags and if changed posts a flagsChanged notification
    func flagsChanged()
    
    /// Initialize reachability service with host name
    ///
    /// - parameter hostname: A host name url string to be initialized
    init?(hostname: String)
    
    /// Initialize reachability service with zero address socket
    ///
    init?()
}
