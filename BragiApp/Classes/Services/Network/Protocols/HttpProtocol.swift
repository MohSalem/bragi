//
//  HttpProtocol.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Http Protocol provides interface for http url session services

protocol HttpProtocol {
    
    /// Http request url get handler.
    ///
    /// - parameter URL: The request’s url to get data from backend.
    /// - parameter completionHandler: The response completion block handler to handle response and error.
    func get(url: URL?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void)
}
