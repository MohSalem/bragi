//
//  AlertProtocol.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Alert Protocol provides interface for presenting alert controller services

protocol AlertProtocol {
    
    /// Present alert controller with title and message
    ///
    /// - parameter title: Title string for alert controller to be presented.
    /// - parameter message: Message string for alert controller to be presented.
    func show(title: String, message: String)
}
