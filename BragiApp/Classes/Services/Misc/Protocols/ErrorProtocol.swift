//
//  ErrorProtocol.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Error Protocol provides interface for presenting alert controller services

protocol ErrorProtocol {
    
    /// Present alert controller with error localized description
    ///
    /// - parameter error: Error of which localized description string to be presented in alert controller.
    func show(error: Error)
    
    /// Print error localized description
    ///
    /// - parameter error: Error of which localized description string to be printed in log window.
    func print(error: Error)
}
