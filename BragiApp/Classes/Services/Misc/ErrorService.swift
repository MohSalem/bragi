//
//  ErrorService.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Error Service provides implementation for presenting alert controller protocol

class ErrorService: ErrorProtocol {
    
    private let alertService: AlertProtocol? = AlertService()
    
    /// Present alert controller with error localized description
    ///
    /// - parameter error: Error of which localized description string to be presented in alert controller.
    func show(error: Error) {
        
        let title = "ErrorAlertControllerTitle".localized()
        let message = error.localizedDescription
        
        alertService?.show(title: title, message: message)
    }
    
    /// Print error localized description
    ///
    /// - parameter error: Error of which localized description string to be printed in log window.
    func print(error: Error) {
        
        let title = "ErrorAlertControllerTitle".localized()
        let message = error.localizedDescription
        
        let log = String.localize(key: "ErrorLogDescription", title, message)
        
        NSLog(log)
    }
}
