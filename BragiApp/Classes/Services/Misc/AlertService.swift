//
//  AlertService.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation
import UIKit

/// Alert Service provides implementation for presenting alert controller protocol

class AlertService: AlertProtocol {
    
    /// Present alert controller with title and message
    ///
    /// - parameter title: Title string for alert controller to be presented.
    /// - parameter message: Message string for alert controller to be presented.
    func show(title: String, message: String) {
        
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "AlertControllerCancellationButtonTitle".localized(), style: .default)
        alertController.addAction(alertAction)
        
        alertController.show()
    }
    
}
