//
//  ChecklistService.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Checklist Service provides implementation for checklist utilities protocol

class ChecklistService: ChecklistProtocol {
    
    private let dataService: DataProtocol? = DataService()
    private let errorService: ErrorProtocol? = ErrorService()
    
    /// Http fetch checklist request url get handler.
    ///
    /// - parameter completionHandler: The response completion block handler to handle checklist deserialized object.
    func checklist(completionHandler: @escaping (Checklist?) -> Void) {
        
        dataService?.fetchChecklist(completionHandler: { (data, response, error) in
            
            if let error = error {
                self.errorService?.show(error: error)
            }
            
            if let data = data {
                do {
                    let checklist = try Checklist(data: data)
                    completionHandler(checklist)
                }
                catch {
                    self.errorService?.show(error: error)
                }
            }
        })
    }
}
