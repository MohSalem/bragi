//
//  ChecklistProtocol.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Checklist Protocol provides interface for checklist utilities services

protocol ChecklistProtocol {
    
    /// Http fetch checklist request url get handler.
    ///
    /// - parameter completionHandler: The response completion block handler to handle checklist deserialized object.
    func checklist(completionHandler: @escaping (Checklist?) -> Void)
}
