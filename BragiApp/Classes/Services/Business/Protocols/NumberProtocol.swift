//
//  NumberProtocol.swift
//  BragiApp
//
//  Created by Mohamed Salem on 10.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Number Protocol provides interface for checklist numbers calculation services

protocol NumberProtocol {
    
    /// Process binary number encapsulating value for checklist section.
    ///
    /// - parameter number: The binary number encapsulating value for checklist section.
    /// - returns: The processed section index for checklist.
    func section(for number: Int) -> Int
    
    /// Process binary number encapsulating value for checklist item.
    ///
    /// - parameter number: The binary number encapsulating value for checklist item.
    /// - returns: The processed item index for checklist.
    func item(for number: Int) -> Int
    
    /// Process binary number encapsulating value for checklist checkmark.
    ///
    /// - parameter number: The binary number encapsulating value for checklist checkmark.
    /// - returns: The processed checkmark bool for checklist.
    func checkmark(for number: Int) -> Bool
}
