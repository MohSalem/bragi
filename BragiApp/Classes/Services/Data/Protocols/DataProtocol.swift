//
//  DataProtocol.swift
//  BragiApp
//
//  Created by Mohamed Salem on 07.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Data Protocol provides interface for http checklist fetch services

protocol DataProtocol {
    
    /// Http fetch checklist request url get handler.
    ///
    /// - parameter completionHandler: The response completion block handler to handle checklist response and error.
    func fetchChecklist(completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void)
}
