//
//  DeserializationProtocol.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Deserialization Protocol provides interface for json deserialization services

protocol DeserializationProtocol {
    
    /// Generically deserialize an value from a given json data.
    ///
    /// - parameter data: a json data
    /// - parameter key: key in the dictionary
    /// - returns: The expected value
    /// - throws: JSONDeserializationError
    func deserialize<T>(_ data: Data, key: String) throws -> T
}
