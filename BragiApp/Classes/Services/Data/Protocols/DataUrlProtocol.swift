//
//  DataUrlProtocol.swift
//  BragiApp
//
//  Created by Mohamed Salem on 07.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Data Url Protocol provides interface for http data urls services

protocol DataUrlProtocol {
    
    var base: String { get }            /// A String property to get base url string for data url services
    var checklistUrl: URL? { get }      /// A URL optional property to get checklist url for data url services
}
