//
//  DataService.swift
//  BragiApp
//
//  Created by Mohamed Salem on 07.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Data Service provides implementation for http checklist fetch protocol

class DataService: DataProtocol {
    
    private let httpService: HttpProtocol? = HttpService()
    private let dataUrlService: DataUrlProtocol? = DataUrlService()
    
    /// Http fetch checklist request url get handler.
    ///
    /// - parameter completionHandler: The response completion block handler to handle checklist response and error.
    func fetchChecklist(completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        let url = dataUrlService?.checklistUrl
        httpService?.get(url: url, completionHandler: completionHandler)        
    }
}
