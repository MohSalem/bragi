//
//  DataUrlService.swift
//  BragiApp
//
//  Created by Mohamed Salem on 07.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Data Url Service provides implementation for http data urls protocol

class DataUrlService: DataUrlProtocol {
    
    /// A String property to get base url string for data url services
    var base: String {
        return "http://foo.bragi.net/"
    }
    
    /// A URL optional property to get checklist url for data url services
    var checklistUrl: URL? {
        let checklist = "numbers.json"
        return URL(string: base + checklist)
    }
}
