//
//  DeserializationService.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Deserialization Service provides implementation for json deserialization protocol

class DeserializationService: DeserializationProtocol {
    
    /// Generically deserialize an value from a given json data.
    ///
    /// - parameter data: a json data
    /// - parameter key: key in the dictionary
    /// - returns: The expected value
    /// - throws: JSONDeserializationError
    func deserialize<T>(_ data: Data, key: String) throws -> T {
        
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject],
            let value = dictionary[key] else {
            throw DeserializationError.missingAttribute(key: key)
        }
        
        guard let attribute = value as? T else {
            throw DeserializationError.invalidAttributeType(key: key, expectedType: T.self, receivedValue: value)
        }
        
        return attribute
    }
    
    /// Decode a serialized object type from a given json data.
    ///
    /// - parameter data: a json data
    /// - parameter key: key in the dictionary
    /// - returns: The expected JSONDeserializable value
    /// - throws: JSONDeserializationError
    private func deserialize<T: Deserializable>(_ data: Data, key: String) throws -> T {
        let value: [String: Any] = try deserialize(data, key: key)
        return try deserialize(value)
    }
    
    /// Decode an array of a serialized object types from a given json data.
    ///
    /// - parameter data: a json data
    /// - parameter key: key in the dictionary
    /// - returns: The expected JSONDeserializable value
    /// - throws: JSONDeserializationError
    private func deserialize<T: Deserializable>(_ data: Data, key: String) throws -> [T] {
        let values: [[String: Any]] = try deserialize(data, key: key)
        return values.flatMap { try? deserialize($0) }
    }
    
    /// Deserialize a serialized object type
    ///
    /// - parameter data: a json data
    /// - returns: the deserialized type
    /// - throws: JSONDeserializationError
    private func deserialize<T: Deserializable>(_ dictionary: [String: Any]) throws -> T {
        let data = try JSONSerialization.data(withJSONObject: dictionary, options: [])
        return try T.init(data: data)
    }
}


