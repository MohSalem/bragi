//
//  Styles.swift
//  BragiApp
//
//  Created by Mohamed Salem on 13.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation
import UIKit

struct Style {
    
    struct cell {
        
        static let transparent: UIViewStyle<UITableViewCell> = UIViewStyle { cell in
            cell.contentView.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.clear
        }
    }
    
    struct tableView {
        
        static let transparent: UIViewStyle<UITableView> = UIViewStyle { tableView in
            tableView.backgroundColor = UIColor.clear
            tableView.separatorInset = UIEdgeInsets.zero
            tableView.allowsSelection = false
        }
    }
    
    struct label {
        
        static let header: UIViewStyle<UILabel> = UIViewStyle { label in
            label.textColor = UIColor.white
            label.font = UIFont.wide(size: 20)
        }
        
        static let checklist: UIViewStyle<UILabel> = UIViewStyle { label in
            label.textColor = UIColor.black
            label.font = UIFont.thin(size: 20)
        }
        
        static let correct: UIViewStyle<UILabel> = UIViewStyle { label in
            label.textColor = UIColor.correct
            label.font = UIFont.wide(size: 40)
            label.textAlignment = .center
        }
        
        static let wrong: UIViewStyle<UILabel> = UIViewStyle { label in
            label.textColor = UIColor.wrong
            label.font = UIFont.wide(size: 32)
            label.textAlignment = .center
        }
    }
    
    struct visualEffectView {
        
        static let dark: UIViewStyle<UIVisualEffectView> = UIViewStyle { visualEffectView in
            visualEffectView.effect = UIBlurEffect(style: .dark)
        }
        
        static let extraLight: UIViewStyle<UIVisualEffectView> = UIViewStyle { visualEffectView in
            visualEffectView.effect = UIBlurEffect(style: .extraLight)
        }
    }
}
