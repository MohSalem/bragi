//
//  Fonts.swift
//  BragiApp
//
//  Created by Mohamed Salem on 13.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    static func thin(size: CGFloat) -> UIFont? {
        return UIFont(name: "MarkerFelt-Thin", size: size)
    }
    
    static func wide(size: CGFloat) -> UIFont? {
        return UIFont(name: "MarkerFelt-Wide", size: size)
    }
}
