//
//  Colors.swift
//  BragiApp
//
//  Created by Mohamed Salem on 13.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static var fade: UIColor {
        get {
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7)
        }
    }
    
    static var correct: UIColor {
        get {
            return #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        }
    }
    
    static var wrong: UIColor {
        get {
            return #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        }
    }
}
