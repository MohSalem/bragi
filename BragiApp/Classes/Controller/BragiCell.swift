//
//  BragiCell.swift
//  BragiApp
//
//  Created by Mohamed Salem on 13.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import UIKit

/// Bragi Cell

class BragiCell: UITableViewCell {

    // MARK:- View life cycle
    
    override func awakeFromNib() {
       super.awakeFromNib()
        
        initViews()
        applyStyles()
        layoutSubview()
    }
    
    // MARK: View initializers
    
    /// Initialize sub views properties of view controller
    func initViews() {
    }
    
    /// Apply styles to sub views of view controller
    func applyStyles() {
    }
    
    /// Adjust layout constraints for sub view of view controller for different screen sizes
    private final func layoutSubview() {
    }
}
