//
//  BragiViewController.swift
//  BragiApp
//
//  Created by Mohamed Salem on 06.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import UIKit

/// Bragi View Controller is an abstract view controller super class for view controllers

class BragiViewController: UIViewController {

    // MARK: Variable properties
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        applyStyles()
        layoutSubview()
    }
    
    // MARK: View initializers
    
    /// Initialize sub views properties of view controller
    func initViews() {
    }
    
    /// Apply styles to sub views of view controller
    func applyStyles() {
    }
    
    /// Adjust layout constraints for sub view of view controller for different screen sizes
    private final func layoutSubview() {
    }
}

