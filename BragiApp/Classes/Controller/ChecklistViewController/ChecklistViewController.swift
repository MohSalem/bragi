//
//  ChecklistViewController.swift
//  BragiApp
//
//  Created by Mohamed Salem on 06.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import UIKit

/// Checklist View Controller

class ChecklistViewController: BragiViewController {

    // MARK: View model initialization
    
    let viewModel = ChecklistViewModel()
    
    // MARK: IB outlet properties
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Variable properties
    
    var tableViewItems: [SectionViewItem]? {
        return viewModel.tableViewItems
    }
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: View initializers
    
    override func initViews() {
        super.initViews()
        
        initTableView()
    }
    
    override func applyStyles() {
        super.applyStyles()
        
        tableView.apply(style: Style.tableView.transparent)
    }
    
    // MARK: Private functions
    
    func initTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
}
