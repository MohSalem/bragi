//
//  ChecklistHeader.swift
//  BragiApp
//
//  Created by Mohamed Salem on 13.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import UIKit

/// Checklist Cell

class ChecklistHeader: BragiCell {

    // MARK: IB outlet properties
    
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: Variable properties
    
    var item: SectionViewItem? {
        didSet {
            titleLabel.text = item?.title
        }
    }
    
    // MARK:- View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: View initializers
    
    override func initViews() {
        super.initViews()
    }
    
    override func applyStyles() {
        super.applyStyles()
        
        self.apply(style: Style.cell.transparent)
        titleLabel.apply(style: Style.label.header)
        visualEffectView.apply(style: Style.visualEffectView.dark)
    }
}
