//
//  ChecklistViewController+UITableViewDataSource.swift
//  BragiApp
//
//  Created by Mohamed Salem on 13.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import UIKit

/// Checklist view controller extension implements table view data source

extension ChecklistViewController: UITableViewDataSource {
    
    // MARK: Section and row count
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        guard let count = tableViewItems?.count else {
            return 0
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let rowViewItems = tableViewItems?[section].items else {
            return 0
        }
        
        return rowViewItems.count
    }
    
    // MARK: Section and row height
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    // MARK: Section and row initialization
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "ChecklistHeader") as! ChecklistHeader
        
        let sectionViewItem = tableViewItems?[section]
        
        header.item = sectionViewItem
        
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistCell", for: indexPath) as! ChecklistCell
        
        let rowViewItems = tableViewItems?[indexPath.section].items
        let item = rowViewItems?[indexPath.row]
        
        cell.item = item
        
        return cell
    }
}
