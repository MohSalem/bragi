//
//  ChecklistCell.swift
//  BragiApp
//
//  Created by Mohamed Salem on 13.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import UIKit

/// Checklist Cell

class ChecklistCell: BragiCell {

    // MARK: IB outlet properties
    
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkmarkLabel: UILabel!
    
    // MARK: Variable properties
    
    var item: RowViewItem? {
        didSet {
            titleLabel.text = item?.title
            if let isChecked = item?.isChecked {
                self.isChecked = isChecked
            }
        }
    }
    
    var isChecked: Bool = false {
        didSet {
            if isChecked {
                checkmarkLabel.text = "\u{2713}"
                checkmarkLabel.apply(style: Style.label.correct)
            }
            else {
                checkmarkLabel.text = "\u{2717}"
                checkmarkLabel.apply(style: Style.label.wrong)
            }
        }
    }
    
    // MARK:- View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: View initializers
    
    override func initViews() {
        super.initViews()
    }
    
    override func applyStyles() {
        super.applyStyles()
        
        self.apply(style: Style.cell.transparent)
        visualEffectView.apply(style: Style.visualEffectView.extraLight)
        titleLabel.apply(style: Style.label.checklist)
    }
}
