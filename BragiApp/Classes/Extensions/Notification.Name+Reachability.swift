//
//  Notification.Name+Reachability.swift
//  BragiApp
//
//  Created by Mohamed Salem on 12.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Notification.Name Extension of flag changed for reachability service notification name.

extension Notification.Name {
    
    /// Notification name for flag changed for reachability service
    static let flagsChanged = Notification.Name("FlagsChanged")
}
