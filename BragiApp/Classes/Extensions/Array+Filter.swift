//
//  Array+Filter.swift
//  BragiApp
//
//  Created by Mohamed Salem on 10.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Array Extension of hashable elements to sort an array without duplicates.

extension Array where Element:Hashable {
    
    /// An Array of ordered set of hashable elements to sort an array without duplicates.
    var orderedSet: Array {
        var set = Set<Element>()
        return filter { element in
            return set.insert(element).inserted
        }
    }
}
