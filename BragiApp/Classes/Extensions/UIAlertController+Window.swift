//
//  UIAlertController+Window.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation
import UIKit

/// UIAlertController Extension to show the alert controller from UIWindow root view controller

extension UIAlertController {
    
    /// Method to present alert controller animated from UIWindow.
    func show() {
        present(animated: true, completion: nil)
    }
    
    /// Method to present alert controller animated from UIWindow.
    ///
    /// - parameter animated: A bool value to determine if alert would be shown animated.
    /// - parameter completion: A completion block to handle execution of block of code after presenting alert controller.
    func present(animated: Bool, completion: (() -> Void)?) {
        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController {
            present(from: rootViewController, animated: animated, completion: completion)
        }
    }
    
    /// Method to present alert controller animated from UIWindow.
    /// 
    /// - parameter controller: A controller from which the alert controller would be presented.
    /// - parameter animated: A bool value to determine if alert would be shown animated.
    /// - parameter completion: A completion block to handle execution of block of code after presenting alert controller.
    private func present(from controller: UIViewController, animated: Bool, completion: (() -> Void)?) {
        if let navigationController = controller as? UINavigationController,
            let visibleViewController = navigationController.visibleViewController {
            present(from: visibleViewController, animated: animated, completion: completion)
        }
        else if let tabBarController = controller as? UITabBarController,
            let selectedViewController = tabBarController.selectedViewController {
                present(from: selectedViewController, animated: animated, completion: completion)
        }
        else {
            controller.present(self, animated: animated, completion: completion);
        }
    }
}
