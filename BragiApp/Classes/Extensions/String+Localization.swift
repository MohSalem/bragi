//
//  String+Localization.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// String Extension to simplify localization of strings

public extension String {
    
    /// Swift 3 friendly localization syntax, replaces NSLocalizedString.
    ///
    /// - returns: The localized string.
    func localized() -> String {
        return String.localize(key: self)
    }
    
    /// Swift 3 friendly localization syntax, replaces NSLocalizedString.
    ///
    /// - parameter key: The receiver’s string key to search.
    /// - returns: The localized string.
    static func localize(key: String) -> String {
        let bundle: Bundle = .main
        if let path = bundle.path(forResource: "Base", ofType: "lproj"),
            let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: key, value: nil, table: nil)
        }
        return key
    }
    
    /// Swift 3 friendly localization syntax with format arguments, replaces String(format:NSLocalizedString)
    
    /// - parameter key: The receiver’s string key to search.
    /// - parameter arguments: The receiver’s arguments template into which the remaining argument values are substituted according to given locale information.
    /// - returns: The formatted localized string with arguments.
    static func localize(key: String, _ arguments: CVarArg...) -> String {
        return String(format: localize(key: key), arguments: arguments)
    }
}
