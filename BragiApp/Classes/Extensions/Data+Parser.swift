//
//  Data+Parser.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Data Extension to deserialize and parse data to a mapped property

extension Data {
    
    /// Swift 3 friendly deserialization subscript syntax.
    ///
    /// - parameter key: The receiver’s string key to parse from a deserialized data into a key-value pair dictionary
    /// - returns: The deserialized object property.
    subscript(key: String) -> Any? {
        do {
            let deserializationService: DeserializationProtocol = DeserializationService()
            return try deserializationService.deserialize(self, key: key)
        }
        catch  {
            let errorService: ErrorProtocol = ErrorService()
            errorService.print(error: error)
            return nil
        }
    }
}
