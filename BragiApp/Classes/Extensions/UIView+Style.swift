//
//  UIView+Style.swift
//  BragiApp
//
//  Created by Mohamed Salem on 13.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation
import UIKit

/// UIView Extension to apply a custom view style to a view.

extension UIView {
    
    /// Method to apply a custom view style to a view.
    ///
    /// - parameter style: A generic UIViewStyle which will be applied to view.
    func apply<T>(style: UIViewStyle<T>) where T: UIView {
        style.apply(to: self as! T)
    }
}
