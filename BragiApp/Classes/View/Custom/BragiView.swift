//
//  BragiView.swift
//  BragiApp
//
//  Created by Mohamed Salem on 06.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import UIKit

/// Bragi View

class BragiView: UIView {

    // MARK:- Class initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    // MARK: View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initViews()
        applyStyles()
    }
    
    // MARK: View initializers
    
    /// Instantiate and setup custom view properties
    private func setup() {
        
        let view = loadViewFromNib()
        
        view.frame = bounds
        view.backgroundColor = UIColor.clear
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        insertSubview(view, at: 0)
    }
    
    /// Load and instantiate custom view from nib file of bundle
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nibName = String(describing: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    /// Initialize sub views properties of custom view
    func initViews() {
    }
    
    /// Apply styles to sub views of custom view
    func applyStyles() {
    }
}
