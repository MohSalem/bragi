//
//  BackgroundView.swift
//  BragiApp
//
//  Created by Mohamed Salem on 06.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import UIKit

/// IBDesignable Background View

@IBDesignable class BackgroundView: BragiView {

    // MARK: IB outlet properties
    
    @IBOutlet private weak var imageView: UIImageView!
    
    // MARK: Variable properties
    
    /// a UIImage inspectable property to determince image for background view
    @IBInspectable var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    // MARK:- View life cycle
    
    override func initViews() {
        super.initViews()
        
        imageView.image = #imageLiteral(resourceName: "background")
    }
}
