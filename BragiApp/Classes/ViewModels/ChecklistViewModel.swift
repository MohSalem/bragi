//
//  ChecklistViewModel.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Checklist View Model for checklist view controller

class ChecklistViewModel: BragiViewModel {
    
    private let checklistService: ChecklistProtocol = ChecklistService()
    
    // MARK: Variable properties
    
    /// Checklist object model represent a checklist with array of numbers retrieved from backend
    var checklist: Checklist? {
        didSet {
            checklistViewItems = initChecklistViewItems(with: checklist)
        }
    }
    
    /// Checklist View Item encapsulates a deserialized number object model
    var checklistViewItems: [ChecklistViewItem]? {
        didSet {
            tableViewItems = initTableViewItems(with: checklistViewItems)
        }
    }
    
    /// Section View Item represents table section for checklist view item
    var tableViewItems: [SectionViewItem]?
    
    // MARK:- Class initializers
    
    override init() {
        super.init()
        
        self.initChecklist()
    }
    
    // MARK: View model initializers
    
    /// Initialize checklist object model with array of numbers retrieved from backend
    private func initChecklist() {
        
        checklistService.checklist(completionHandler: { (checklist) in
            self.checklist = checklist
        })
    }
    
    /// Initialize Checklist View Items that encapsulates a checklist object model
    private func initChecklistViewItems(with checklist: Checklist?) -> [ChecklistViewItem]? {
        
        guard let numbers = checklist?.numbers else { return nil }
        
        var checklistViewItems: [ChecklistViewItem] = []
        
        for number in numbers {
            
            let checklistViewItem = ChecklistViewItem(number: number)
            checklistViewItems.append(checklistViewItem)
        }
        
        return checklistViewItems.sorted(by: { (item1, item2) -> Bool in
            
            if item1.section == item2.section {
                return item1.item < item2.item
            }
            
            return item1.section < item2.section
        })
    }
    
    /// Initialize section and row view Items that represents table section for checklist view item
    private func initTableViewItems(with checklistViewItems: [ChecklistViewItem]?) -> [SectionViewItem]? {
        
        guard let checklistViewItems = checklistViewItems else { return nil }

        let sections = checklistViewItems.map { $0.section }.orderedSet
        
        var sectionViewItems: [SectionViewItem] = []
        
        for section in sections {
            
            let sectionViewItem = SectionViewItem(index: section, items: checklistViewItems)
            sectionViewItems.append(sectionViewItem)
        }
        
        return sectionViewItems
    }
}
