//
//  DeserializationError.swift
//  BragiApp
//
//  Created by Mohamed Salem on 09.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Errors for deserializing JSON representations

public enum DeserializationError: LocalizedError {
    
    /// A required attribute was missing
    case missingAttribute(key: String)
    
    /// An invalid type for an attribute was found
    case invalidAttributeType(key: String, expectedType: Any.Type, receivedValue: Any)
    
    /// An attribute was invalid
    case invalidAttribute(key: String)
    
    /// Override localized error description for error alert controller
    var localizedDescription: String {
        return "DeserializationErrorMessage".localized()
    }
}
