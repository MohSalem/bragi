//
//  ReachabilityError.swift
//  BragiApp
//
//  Created by Mohamed Salem on 12.09.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

/// Errors for connection reachability service

public enum ReachabilityError: Error {
    
    /// Failed to set callout for reachabilty service
    case failedToSetCallout
    
    /// Failed to set dispatch queue for reachabilty service
    case failedToSetDispatchQueue
    
    /// Not reachable connection reachability status
    case unreachable
    
    /// Override localized error description for error alert controller
    var localizedDescription: String {
        return "NoInternetErrorMessage".localized()
    }
}
