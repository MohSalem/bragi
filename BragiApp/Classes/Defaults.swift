//
//  Defaults.swift
//  BragiApp
//
//  Created by Mohamed Salem on 17.08.17.
//  Copyright © 2017 Bragi. All rights reserved.
//

import Foundation

import Foundation
import UIKit

// MARK: - Project defaults

/// Static defaults go here

struct Defaults {
    
    /// Constants related to projectå
}

// MARK: - Screen size constants

struct Screen {
    
    /// Constants related to screen size
    
    static let width = UIScreen.main.bounds.size.width
    static let height = UIScreen.main.bounds.size.height
    static let maxLength = max(Screen.width, Screen.height)
    static let minLength = min(Screen.width, Screen.height)
    
    static let navigationBarHeight = CGFloat(44)
    static let statusBarHeight = CGFloat(20)
    
    static let ratio = (Device.phone) ? maxLength / 667.0 : maxLength / 1024.0
}

// MARK: - Device type constants

struct Device {
    
    /// Constants related to device types
    
    static let phone =  UIDevice.current.userInterfaceIdiom == .phone
    static let pad = UIDevice.current.userInterfaceIdiom == .pad
    static let tv = UIDevice.current.userInterfaceIdiom == .tv
    static let carPlay = UIDevice.current.userInterfaceIdiom == .carPlay
    
    static let iPhone4 =  phone && Screen.maxLength < 568.0
    static let iPhone5 = phone && Screen.maxLength == 568.0
    static let iPhone6 = phone && Screen.maxLength == 667.0
    static let iPhone6P = phone && Screen.maxLength == 736.0
    
    static let iPad = pad && Screen.maxLength == 1024.0
    static let iPadPro = pad && Screen.maxLength == 1366.0
    
    static let Retina = UIScreen.main.scale >= 2.0
}

// MARK: - System version constants

struct SystemVersion {
    
    /// Constants related to system version
    
    static let major = ProcessInfo.processInfo.operatingSystemVersion.majorVersion
    static let minor = ProcessInfo.processInfo.operatingSystemVersion.minorVersion
    static let patch = ProcessInfo.processInfo.operatingSystemVersion.patchVersion
    
    static let iOS8 = major == 8
    static let iOS9 = major == 9
    static let iOS10 = major == 10
    static let iOS11 = major == 11
}
